include(bookworm)

RUN apt install --yes wget nginx ssl-cert

### Install minio
RUN wget https://dl.min.io/server/minio/release/linux-amd64/minio && \
    chmod +x minio && \
    mv minio /usr/local/bin/

### Install minio client
RUN wget https://dl.min.io/client/mc/release/linux-amd64/mc && \
    chmod +x mc	&& \
    mv mc /usr/local/bin/
