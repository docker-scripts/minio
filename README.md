# minio container

## Installation

  - First install `ds` and `revproxy`:
     + https://gitlab.com/docker-scripts/ds#installation
     + https://gitlab.com/docker-scripts/revproxy#installation

  - Then get the scripts: `ds pull minio`

  - Create a directory for the container: `ds init minio @minio.example.org`

  - Fix the settings: `cd /var/ds/minio.example.org/; vim settings.sh`

  - Make it: `ds make`
  
  - Open in browser: https://minio.example.org


## Usage

Manage users:
```bash
ds user
ds user add user1 pass1234
ds user add user2 pass1234
ds user list
ds user del user1
ds user list
```
