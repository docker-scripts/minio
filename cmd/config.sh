cmd_config_help() {
    cat <<_EOF
    config
        Run configuration scripts inside the container.

_EOF
}

cmd_config() {
    ds inject msmtp.sh
    ds inject logwatch.sh $(hostname)

    ds inject minio.sh
    ds inject nginx.sh
}
