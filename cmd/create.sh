cmd_create_help() {
    cat <<_EOF
    create
        Create the container '$CONTAINER'.

_EOF
}

rename_function cmd_create orig_cmd_create
cmd_create() {
    chmod +x .
    mkdir -p data
    orig_cmd_create \
        --mount type=bind,src=$(pwd)/data,dst=/data
}
