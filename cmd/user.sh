cmd_user_help() {
    cat << _EOF
    user <cmd> [<arguments>]
        Simple command to manage minio users.
        It can be used like this:
        - add <username> <password>
        - del <username>
        - list

_EOF
}

cmd_user() {
    local cmd=$1; shift
    case $cmd in
        add)
            _user_add "$@"
            ;;
        del)
            _user_del "$@"
            ;;
        list)
            _user_list
            ;;
        *)
            _user_fail
            ;;
    esac
}

_user_fail() {
    fail "Usage:\n$(cmd_user_help)"
}

_user_list() {
    _mc admin user list local
}

_mc() {
    ds shell mc "$@"
}

_user_del() {
    local user=$1
    [[ -n $user ]] || _user_fail

    # remove the user, its policy and its bucket
    _mc admin user remove local $user
    _mc admin policy remove local $user-policy
    _mc rb --force local/$user
}

_user_add() {
    local user=$1
    local pass=$2
    [[ -n $user ]] || _user_fail
    [[ -n $pass ]] || _user_fail

    # create a bucket for the user
    _mc mb --ignore-existing local/$user
    
    # create a policy for the user
    cat <<EOF > $user.json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AllowBucketStat",
            "Effect": "Allow",
            "Action": [
                "s3:HeadBucket"
            ],
            "Resource": "arn:aws:s3:::*"
        },
        {
            "Sid": "AllowThisBucketOnly",
            "Effect": "Allow",
            "Action": [ "s3:*" ],
            "Resource": [
                "arn:aws:s3:::$user",
                "arn:aws:s3:::$user/*"
            ]
        }
    ]
}
EOF
    _mc admin policy add local $user-policy /host/$user.json
    rm $user.json

    # add the user and set the policy
    _mc admin user add local $user $pass
    _mc admin policy set local $user-policy user=$user
}
