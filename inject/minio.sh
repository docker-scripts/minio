#!/bin/bash -x

source /host/settings.sh

main() {
    create_minio_user
    setup_config
    create_systemd_service
    sleep 5
    mc alias set local http://localhost:9000 "$MINIO_ROOT_USER" "$MINIO_ROOT_PASSWORD"
}

create_minio_user() {
    groupadd -r minio-user
    useradd -M -r -g minio-user minio-user
    chown minio-user:minio-user -R /data
}

setup_config() {
    cat <<EOF > /etc/default/minio
MINIO_ROOT_USER="$MINIO_ROOT_USER"
MINIO_ROOT_PASSWORD="$MINIO_ROOT_PASSWORD"
MINIO_VOLUMES="/data"
MINIO_OPTS="--address :9000 --console-address :9001"
MINIO_CONFIG_ENV_FILE=/etc/default/minio
EOF
}

create_systemd_service() {
    wget https://raw.githubusercontent.com/minio/minio-service/master/linux-systemd/minio.service
    mv minio.service /etc/systemd/system/
    systemctl enable minio
    systemctl start minio
}

# call the main function
main "$@"
