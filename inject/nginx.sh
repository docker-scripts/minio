#!/bin/bash -x

cat <<'EOF' > /etc/nginx/sites-available/minio
server {
  listen 80;
  listen 443 ssl;
  server_name minio.example.org;

  ssl_protocols TLSv1.2 TLSv1.3;
  ssl_ciphers HIGH:!MEDIUM:!LOW:!aNULL:!NULL:!SHA;
  ssl_prefer_server_ciphers on;
  ssl_session_cache shared:SSL:10m;
  ssl_session_tickets off;

  ssl_certificate     /etc/ssl/certs/ssl-cert-snakeoil.pem;
  ssl_certificate_key /etc/ssl/private/ssl-cert-snakeoil.key;

  ignore_invalid_headers off;
  proxy_buffering off;

  # Allow any size file to be uploaded.
  # Set to a value such as 1000m; to restrict file size to a specific value
  client_max_body_size 0;

  location / {
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_set_header Host $http_host;

    proxy_connect_timeout 300;
    proxy_http_version 1.1;
    proxy_set_header Connection "";
    chunked_transfer_encoding off;

    proxy_pass http://localhost:9001;
   }
}
EOF

source /host/settings.sh
sed -i /etc/nginx/sites-available/minio \
    -e "s/minio\.example\.org/$DOMAIN/"

ln -s /etc/nginx/sites-available/minio /etc/nginx/sites-enabled/minio
rm /etc/nginx/sites-enabled/default

systemctl restart nginx
